﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RA;
using TechTalk.SpecFlow;

//Author : Nancy Malgani

namespace UnitTestProject4
{
    [Binding]
    [TestClass]
    public class HomeSteps
    {
        private HomePage homePage = new HomePage();

        [Given(@"I launch the website")]
        public void GivenILaunchTheWebsite()
        {
            homePage.LaunchWebsite();
        }

        [When(@"I navigate to Contact Synergy Page")]
        public void WhenINavigateToContactUs()
        {
            homePage.NavigateToContactUs();
        }
        

        [Then(@"I should see section for '(.*)'")]
        public void ThenIShouldSeeSectionFor(string p0)
        {
            homePage.VerifySectionIsPresent(p0);
        }

        [Then(@"I should close the browser")]
        public void ThenIShouldCloseTheBrowser()
        {
            homePage.TearDownAfterScenario();
        }
        
        [AfterScenario]
        public void TearDown()
        {
            //homePage.TearDownAfterScenario();
        }

    }
}

