﻿Feature: WebsiteDemo
	In order to ensure clients can contact us
	As a tester
	I want to verify presence of mandatory fields on contact synergy page

@WebsiteTesting
Scenario: Verify Contact Synergy page
	Given I launch the website
	When I navigate to Contact Synergy Page
	Then I should see section for 'General Contact'
	And I should see section for 'Web Support'
	And I should see section for 'Email Support'
	And I should close the browser