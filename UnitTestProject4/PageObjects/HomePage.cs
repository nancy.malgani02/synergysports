﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

//Author : Nancy Malgani

namespace UnitTestProject4
{
    public class HomePage
    {
        IWebDriver driver;
        private readonly By ButtonContactUs = By.XPath("//nav//a[text()='Contact Synergy']");

        public void LaunchWebsite()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://corp.synergysportstech.com/");
            driver.Manage().Window.Maximize();
        }

        public void NavigateToContactUs()
        {
            driver.FindElement(ButtonContactUs).Click();
        }

        public void ScrollToBottomOfPage()
        {
            ((IJavaScriptExecutor)driver).ExecuteScript("window.scrollTo(0, document.body.scrollHeight - 500)");
        }
        

        public void VerifySectionIsPresent(string p0)
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Assert.IsTrue(body.Text.Contains(p0));
        }
        

        public void TearDownAfterScenario()
        {
            driver.Quit();
        }
    }
}

