﻿using System.ComponentModel;
using System.Windows.Forms;

namespace TestRunnerWin
{
    partial class RunnerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnChooseConfig = new System.Windows.Forms.Button();
            this.txtConfig = new System.Windows.Forms.TextBox();
            this.lblConfigFile = new System.Windows.Forms.Label();
            this.rtbOutput = new System.Windows.Forms.RichTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.btnChooseProject = new System.Windows.Forms.Button();
            this.btnRun = new System.Windows.Forms.Button();
            this.lblDLL = new System.Windows.Forms.Label();
            this.txtDLL = new System.Windows.Forms.TextBox();
            this.btnChooseDLL = new System.Windows.Forms.Button();
            this.lblSpecflow = new System.Windows.Forms.Label();
            this.txtTagsInclude = new System.Windows.Forms.TextBox();
            this.chkAutoOpen = new System.Windows.Forms.CheckBox();
            this.btnClearLog = new System.Windows.Forms.Button();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTagsExclude = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkSpecFlowReport = new System.Windows.Forms.CheckBox();
            this.chkNunitReport = new System.Windows.Forms.CheckBox();
            this.chkArtifactOverride = new System.Windows.Forms.CheckBox();
            this.txtArtifactSubDir = new System.Windows.Forms.TextBox();
            this.llbTagHelp = new System.Windows.Forms.LinkLabel();
            this.txtAppiumURI = new System.Windows.Forms.TextBox();
            this.chkAppiumURI = new System.Windows.Forms.CheckBox();
            this.chkReportUnit = new System.Windows.Forms.CheckBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnChooseConfig
            // 
            this.btnChooseConfig.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseConfig.Location = new System.Drawing.Point(635, 21);
            this.btnChooseConfig.Name = "btnChooseConfig";
            this.btnChooseConfig.Size = new System.Drawing.Size(75, 23);
            this.btnChooseConfig.TabIndex = 0;
            this.btnChooseConfig.Text = "Select";
            this.btnChooseConfig.UseVisualStyleBackColor = true;
            this.btnChooseConfig.Click += new System.EventHandler(this.btnChooseConfig_Click);
            // 
            // txtConfig
            // 
            this.txtConfig.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtConfig.Location = new System.Drawing.Point(152, 22);
            this.txtConfig.Name = "txtConfig";
            this.txtConfig.Size = new System.Drawing.Size(468, 20);
            this.txtConfig.TabIndex = 1;
            // 
            // lblConfigFile
            // 
            this.lblConfigFile.AutoSize = true;
            this.lblConfigFile.Location = new System.Drawing.Point(70, 25);
            this.lblConfigFile.Name = "lblConfigFile";
            this.lblConfigFile.Size = new System.Drawing.Size(79, 13);
            this.lblConfigFile.TabIndex = 2;
            this.lblConfigFile.Text = "Test config file:";
            // 
            // rtbOutput
            // 
            this.rtbOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.rtbOutput.BackColor = System.Drawing.Color.White;
            this.rtbOutput.Location = new System.Drawing.Point(22, 333);
            this.rtbOutput.Name = "rtbOutput";
            this.rtbOutput.ReadOnly = true;
            this.rtbOutput.Size = new System.Drawing.Size(701, 183);
            this.rtbOutput.TabIndex = 3;
            this.rtbOutput.Text = "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(61, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 21);
            this.label1.TabIndex = 6;
            this.label1.Text = "Specflow features csproj file:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtProject
            // 
            this.txtProject.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtProject.Location = new System.Drawing.Point(243, 38);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(351, 20);
            this.txtProject.TabIndex = 5;
            // 
            // btnChooseProject
            // 
            this.btnChooseProject.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseProject.Location = new System.Drawing.Point(613, 37);
            this.btnChooseProject.Name = "btnChooseProject";
            this.btnChooseProject.Size = new System.Drawing.Size(75, 23);
            this.btnChooseProject.TabIndex = 4;
            this.btnChooseProject.Text = "Select";
            this.btnChooseProject.UseVisualStyleBackColor = true;
            this.btnChooseProject.Click += new System.EventHandler(this.btnChooseProject_Click);
            // 
            // btnRun
            // 
            this.btnRun.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRun.Location = new System.Drawing.Point(648, 522);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 7;
            this.btnRun.Text = "Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // lblDLL
            // 
            this.lblDLL.Location = new System.Drawing.Point(80, 53);
            this.lblDLL.Name = "lblDLL";
            this.lblDLL.Size = new System.Drawing.Size(66, 19);
            this.lblDLL.TabIndex = 10;
            this.lblDLL.Text = "Test DLL:";
            this.lblDLL.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtDLL
            // 
            this.txtDLL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDLL.Location = new System.Drawing.Point(152, 52);
            this.txtDLL.Name = "txtDLL";
            this.txtDLL.Size = new System.Drawing.Size(468, 20);
            this.txtDLL.TabIndex = 9;
            // 
            // btnChooseDLL
            // 
            this.btnChooseDLL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnChooseDLL.Location = new System.Drawing.Point(635, 52);
            this.btnChooseDLL.Name = "btnChooseDLL";
            this.btnChooseDLL.Size = new System.Drawing.Size(75, 23);
            this.btnChooseDLL.TabIndex = 8;
            this.btnChooseDLL.Text = "Select";
            this.btnChooseDLL.UseVisualStyleBackColor = true;
            this.btnChooseDLL.Click += new System.EventHandler(this.btnChooseDLL_Click);
            // 
            // lblSpecflow
            // 
            this.lblSpecflow.Location = new System.Drawing.Point(12, 83);
            this.lblSpecflow.Name = "lblSpecflow";
            this.lblSpecflow.Size = new System.Drawing.Size(134, 25);
            this.lblSpecflow.TabIndex = 13;
            this.lblSpecflow.Text = "Include tags/categories:";
            this.lblSpecflow.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTagsInclude
            // 
            this.txtTagsInclude.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTagsInclude.Location = new System.Drawing.Point(152, 82);
            this.txtTagsInclude.Name = "txtTagsInclude";
            this.txtTagsInclude.Size = new System.Drawing.Size(468, 20);
            this.txtTagsInclude.TabIndex = 12;
            // 
            // chkAutoOpen
            // 
            this.chkAutoOpen.AutoSize = true;
            this.chkAutoOpen.Location = new System.Drawing.Point(51, 93);
            this.chkAutoOpen.Name = "chkAutoOpen";
            this.chkAutoOpen.Size = new System.Drawing.Size(186, 17);
            this.chkAutoOpen.TabIndex = 14;
            this.chkAutoOpen.Text = "Open report(s) after run completes";
            this.chkAutoOpen.UseVisualStyleBackColor = true;
            // 
            // btnClearLog
            // 
            this.btnClearLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearLog.Location = new System.Drawing.Point(20, 522);
            this.btnClearLog.Name = "btnClearLog";
            this.btnClearLog.Size = new System.Drawing.Size(75, 23);
            this.btnClearLog.TabIndex = 17;
            this.btnClearLog.Text = "Clear Log";
            this.btnClearLog.UseVisualStyleBackColor = true;
            this.btnClearLog.Click += new System.EventHandler(this.btnClearLog_Click);
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSaveSettings.Location = new System.Drawing.Point(101, 522);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(101, 23);
            this.btnSaveSettings.TabIndex = 19;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(134, 23);
            this.label2.TabIndex = 21;
            this.label2.Text = "Exclude tags/categories:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // txtTagsExclude
            // 
            this.txtTagsExclude.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtTagsExclude.Location = new System.Drawing.Point(152, 111);
            this.txtTagsExclude.Name = "txtTagsExclude";
            this.txtTagsExclude.Size = new System.Drawing.Size(468, 20);
            this.txtTagsExclude.TabIndex = 20;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.chkReportUnit);
            this.groupBox1.Controls.Add(this.chkSpecFlowReport);
            this.groupBox1.Controls.Add(this.chkNunitReport);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnChooseProject);
            this.groupBox1.Controls.Add(this.txtProject);
            this.groupBox1.Controls.Add(this.chkAutoOpen);
            this.groupBox1.Location = new System.Drawing.Point(22, 210);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(701, 117);
            this.groupBox1.TabIndex = 22;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Reporting";
            // 
            // chkSpecFlowReport
            // 
            this.chkSpecFlowReport.AutoSize = true;
            this.chkSpecFlowReport.Location = new System.Drawing.Point(51, 17);
            this.chkSpecFlowReport.Name = "chkSpecFlowReport";
            this.chkSpecFlowReport.Size = new System.Drawing.Size(137, 17);
            this.chkSpecFlowReport.TabIndex = 16;
            this.chkSpecFlowReport.Text = "Create SpecFlow report";
            this.chkSpecFlowReport.UseVisualStyleBackColor = true;
            this.chkSpecFlowReport.CheckedChanged += new System.EventHandler(this.chkSpecFlowReport_CheckedChanged);
            // 
            // chkNunitReport
            // 
            this.chkNunitReport.AutoSize = true;
            this.chkNunitReport.Location = new System.Drawing.Point(51, 63);
            this.chkNunitReport.Name = "chkNunitReport";
            this.chkNunitReport.Size = new System.Drawing.Size(113, 17);
            this.chkNunitReport.TabIndex = 15;
            this.chkNunitReport.Text = "Create nunit report";
            this.chkNunitReport.UseVisualStyleBackColor = true;
            // 
            // chkArtifactOverride
            // 
            this.chkArtifactOverride.AutoSize = true;
            this.chkArtifactOverride.Location = new System.Drawing.Point(152, 140);
            this.chkArtifactOverride.Name = "chkArtifactOverride";
            this.chkArtifactOverride.Size = new System.Drawing.Size(164, 17);
            this.chkArtifactOverride.TabIndex = 23;
            this.chkArtifactOverride.Text = "Override artifact sub directory";
            this.chkArtifactOverride.UseVisualStyleBackColor = true;
            this.chkArtifactOverride.CheckedChanged += new System.EventHandler(this.chkArtifactOverride_CheckedChanged);
            // 
            // txtArtifactSubDir
            // 
            this.txtArtifactSubDir.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtArtifactSubDir.Enabled = false;
            this.txtArtifactSubDir.Location = new System.Drawing.Point(319, 140);
            this.txtArtifactSubDir.Name = "txtArtifactSubDir";
            this.txtArtifactSubDir.Size = new System.Drawing.Size(301, 20);
            this.txtArtifactSubDir.TabIndex = 24;
            // 
            // llbTagHelp
            // 
            this.llbTagHelp.AutoSize = true;
            this.llbTagHelp.Location = new System.Drawing.Point(637, 87);
            this.llbTagHelp.Name = "llbTagHelp";
            this.llbTagHelp.Size = new System.Drawing.Size(67, 13);
            this.llbTagHelp.TabIndex = 25;
            this.llbTagHelp.TabStop = true;
            this.llbTagHelp.Text = "Help on tags";
            this.llbTagHelp.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.llbTagHelp_LinkClicked);
            // 
            // txtAppiumURI
            // 
            this.txtAppiumURI.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtAppiumURI.Enabled = false;
            this.txtAppiumURI.Location = new System.Drawing.Point(319, 168);
            this.txtAppiumURI.Name = "txtAppiumURI";
            this.txtAppiumURI.Size = new System.Drawing.Size(301, 20);
            this.txtAppiumURI.TabIndex = 27;
            // 
            // chkAppiumURI
            // 
            this.chkAppiumURI.AutoSize = true;
            this.chkAppiumURI.Location = new System.Drawing.Point(152, 168);
            this.chkAppiumURI.Name = "chkAppiumURI";
            this.chkAppiumURI.Size = new System.Drawing.Size(125, 17);
            this.chkAppiumURI.TabIndex = 26;
            this.chkAppiumURI.Text = "Override appium URI";
            this.chkAppiumURI.UseVisualStyleBackColor = true;
            this.chkAppiumURI.CheckedChanged += new System.EventHandler(this.chkAppiumURI_CheckedChanged);
            // 
            // chkReportUnit
            // 
            this.chkReportUnit.AutoSize = true;
            this.chkReportUnit.Location = new System.Drawing.Point(170, 63);
            this.chkReportUnit.Name = "chkReportUnit";
            this.chkReportUnit.Size = new System.Drawing.Size(107, 17);
            this.chkReportUnit.TabIndex = 17;
            this.chkReportUnit.Text = "Create report unit";
            this.chkReportUnit.UseVisualStyleBackColor = true;
            // 
            // RunnerForm
            // 
            this.AcceptButton = this.btnRun;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(744, 561);
            this.Controls.Add(this.txtAppiumURI);
            this.Controls.Add(this.chkAppiumURI);
            this.Controls.Add(this.llbTagHelp);
            this.Controls.Add(this.txtArtifactSubDir);
            this.Controls.Add(this.chkArtifactOverride);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTagsExclude);
            this.Controls.Add(this.btnSaveSettings);
            this.Controls.Add(this.lblDLL);
            this.Controls.Add(this.btnClearLog);
            this.Controls.Add(this.btnChooseDLL);
            this.Controls.Add(this.txtDLL);
            this.Controls.Add(this.lblSpecflow);
            this.Controls.Add(this.txtTagsInclude);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.rtbOutput);
            this.Controls.Add(this.lblConfigFile);
            this.Controls.Add(this.txtConfig);
            this.Controls.Add(this.btnChooseConfig);
            this.MinimumSize = new System.Drawing.Size(600, 560);
            this.Name = "RunnerForm";
            this.ShowIcon = false;
            this.Text = "Test Runner";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Button btnChooseConfig;
        private TextBox txtConfig;
        private Label lblConfigFile;
        private RichTextBox rtbOutput;
        private Label label1;
        private TextBox txtProject;
        private Button btnChooseProject;
        private Button btnRun;
        private Label lblDLL;
        private TextBox txtDLL;
        private Button btnChooseDLL;
        private Label lblSpecflow;
        private TextBox txtTagsInclude;
        private CheckBox chkAutoOpen;
        private Button btnClearLog;
        private Button btnSaveSettings;
        private Label label2;
        private TextBox txtTagsExclude;
        private GroupBox groupBox1;
        private CheckBox chkArtifactOverride;
        private TextBox txtArtifactSubDir;
        private CheckBox chkNunitReport;
        private CheckBox chkSpecFlowReport;
        private LinkLabel llbTagHelp;
        private TextBox txtAppiumURI;
        private CheckBox chkAppiumURI;
        private CheckBox chkReportUnit;
        
    }
}

